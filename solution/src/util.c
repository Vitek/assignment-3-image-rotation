//
// Created by vccha on 23.01.2023.
//

#include "util.h"

size_t padding_size(size_t width) {
    return 4 - (width * 3) % 4;
}

size_t bfile_size(const struct image* img) {
    return sizeof(struct bmp_header)+padding_size(img->width)+ image_size(img);
}

size_t bfile_size_from_hw(size_t h, size_t w) {
    return sizeof(struct bmp_header)+padding_size(w)+ w*h*pixel_size();
}

size_t image_size_from_hw(size_t h, size_t w) {
    return w*h*pixel_size();
}
