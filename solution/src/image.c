//
// Created by vccha on 23.01.2023.
//

#include "image.h"

size_t image_size(const struct image* img) {
    return img->height * img->width * pixel_size();
}

struct image create_empty_image(void) {
    return (struct image) {0,0,NULL};
}

struct image create_image(const uint64_t height, const uint64_t width) {
    return (struct image) {height, width, allocate_image_data(height, width)};
}

struct pixel* allocate_image_data(uint64_t height, uint64_t width) {
    struct pixel* data=malloc(pixel_size()*height*width);
    return data;
}

void free_image(struct image* image) {
    if (image && image->data) {
        free(image->data);
        image->data=NULL;
    }
}
