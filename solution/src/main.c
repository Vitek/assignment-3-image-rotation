//
// Created by vccha on 23.01.2023.
//

#include "change_image.h"
#include "from_bmp.h"
#include "image.h"
#include "to_bmp.h"
#include <stdio.h>

static int read_file(FILE *in, struct image* img) {
    if (from_bmp(in, img)) {
        fclose(in);
        free_image(img);
        fprintf(stderr, "Error: can't read bmp file\n");
        return 1;
    }
    fclose(in);
    return 0;
}

static FILE* open_in(char* path) {
    FILE *in = fopen(path, "rb");

    if (!in) {
        fprintf(stderr, "Error: can't open IN file\n");
        return NULL;
    }
    return in;
}

static int write_file(FILE* out, struct image* img) {

    if (to_bmp(out, img)) {
        fclose(out);
        free_image(img);
        fprintf(stderr, "Error: can't write bmp file\n");
        return 1;
    }
    fclose(out);
    return 0;
}

static FILE* open_out(char* path) {
    FILE* out = fopen(path, "wb");

    if (!out) {
        fclose(out);
        fprintf(stderr, "Error: can't open OUT file\n");
        return NULL;
    }
    return out;
}

int main( int argc, char** argv ) {

    if (argc != 3) {
        fprintf(stderr, "Error: 2 arguments expected\n");
        return 1;
    }

    struct image img;

    FILE* in=open_in(argv[1]);
    if (!in) {
        return 1;
    }

    if (read_file(in,&img)) {
        return 1;
    }

    printf("\n-----------------%zu---------------\n",image_size(&img));

    struct image changed = change_image(&img);

    FILE* out=open_out(argv[2]);
    
    if (!out) {
        return 1;
    }

    if (write_file(out,&changed)) {
        return 1;
    }

    free_image(&img);
    free_image(&changed);

    return 0;
}
