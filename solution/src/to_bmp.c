//
// Created by vccha on 23.01.2023.
//

#include "to_bmp.h"

static struct bmp_header create_bmp_header( struct image const *img ) {
    return (struct bmp_header){
            .bfType = BF_TYPE_ST,
            .bfileSize = bfile_size(img),
            .bfReserved = BF_RESERVED_ST,
            .bOffBits = B_OFF_BITS_ST,
            .biSize = BI_SIZE_ST,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BI_PLANES_ST,
            .biBitCount = BI_BIT_COUNT_ST,
            .biCompression = BI_COMPRESSION_ST,
            .biSizeImage = image_size(img),
            .biXPelsPerMeter = BI_X_PELS_PER_METER_ST,
            .biYPelsPerMeter = BI_Y_PELS_PER_METER_ST,
            .biClrUsed = BI_CLR_USED_ST,
            .biClrImportant = BI_CLR_IMPORTANT_ST
    };
}

static enum write_status write_header_to_bmp(FILE* out, struct bmp_header* bmp_header) {
    if (!fwrite(bmp_header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_data_to_bmp(FILE* out, struct image const* img) {
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + img->width * i, img->width * pixel_size(), 1, out)) return WRITE_ERROR;
        if (fseek(out, (int64_t) padding_size(img->width), SEEK_CUR) !=0) return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!img) {
        return WRITE_IMG_NULL;
    }
    struct bmp_header bmp_header = create_bmp_header(img);

    if (write_header_to_bmp(out,&bmp_header)) {
        return WRITE_ERROR;
    }

    return write_data_to_bmp(out, img);
}
