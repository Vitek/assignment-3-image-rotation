//
// Created by vccha on 23.01.2023.
//
#include "pixel.h"

size_t pixel_size(void) {
    return sizeof (struct pixel);
}
