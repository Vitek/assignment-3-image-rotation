//
// Created by vccha on 23.01.2023.
//

#include "change_image.h"

struct image change_image(const struct image* img) {
    const uint64_t height=img->height;
    const uint64_t width=img->width;
    struct image new_img = create_image(width,height);
    if (!new_img.data) {
        return new_img;
    }
    for (size_t i = 0; i < height*width; i++) {
           new_img.data[(i%width + 1) * height - i/width - 1] = img->data[i];
    }
    return new_img;
}
