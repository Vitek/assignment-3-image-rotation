//
// Created by vccha on 23.01.2023.
//

#include "from_bmp.h"

static enum read_status read_header_from_bmp(FILE* in, struct bmp_header* bmp_header) {
    if (!fread(bmp_header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }
    if (bmp_header->bfType != BE_TYPE && bmp_header->bfType != LE_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (bmp_header->bfileSize < bfile_size_from_hw(bmp_header->biHeight, bmp_header->biWidth)) {
        return READ_INVALID_SIGNATURE;
    }
    if (bmp_header->biSizeImage < image_size_from_hw(bmp_header->biHeight, bmp_header->biWidth)) {
        return READ_INVALID_SIGNATURE;
    }
    if (bmp_header->bfReserved != BF_RESERVED_ST) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

static enum read_status read_data_from_bmp(FILE* in, struct image* img) {
    for (size_t i = 0; i < img->height; i++) {
        if (fread(img->data + img->width * i, pixel_size(), img->width, in) != img->width) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, (int64_t) padding_size(img->width), SEEK_CUR)) {
            free_image(img);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    if (!in || !img) {
        return READ_NULL_IN;
    }
    struct bmp_header bmp_header;

    enum read_status read_header_res = read_header_from_bmp(in, &bmp_header);

    if(read_header_res) {
        return read_header_res;
    }

    *img = create_image(bmp_header.biHeight, bmp_header.biWidth);

    if (!img->data) {
        return READ_CREATE_IMAGE_ERROR;
    }

    enum read_status read_data_res = read_data_from_bmp(in, img);

    if(read_data_res) {
        return read_data_res;
    }

    return READ_OK;
}

