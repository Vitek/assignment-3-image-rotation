//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef CHANGE_IMAGE
#define CHANGE_IMAGE

#include "image.h"

struct image change_image(const struct image* img);

#endif
