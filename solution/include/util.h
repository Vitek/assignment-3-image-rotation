//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef UTIL
#define UTIL

#include "bmp_header.h"
#include "image.h"
#include "pixel.h"

size_t padding_size(size_t width);

size_t bfile_size(const struct image* img);

size_t bfile_size_from_hw(size_t h, size_t w);

size_t image_size_from_hw(size_t h, size_t w);

#endif
