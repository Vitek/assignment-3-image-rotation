//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef BMP_HEADER
#define BMP_HEADER

#define LE_TYPE 0x4D42
#define BE_TYPE 0x422D
#define BF_TYPE_ST 0x4D42
#define BF_RESERVED_ST 0
#define B_OFF_BITS_ST sizeof(struct bmp_header)
#define BI_SIZE_ST 40
#define BI_PLANES_ST 1
#define BI_BIT_COUNT_ST 24
#define BI_COMPRESSION_ST 0
#define BI_X_PELS_PER_METER_ST 2834
#define BI_Y_PELS_PER_METER_ST 2834
#define BI_CLR_USED_ST 0
#define BI_CLR_IMPORTANT_ST 0

#include  <stdint.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

#endif
