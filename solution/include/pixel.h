//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef PIXEL
#define PIXEL

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct pixel { uint8_t b, g, r; };

#pragma pack(pop)

size_t pixel_size(void);

#endif

