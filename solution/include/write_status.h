//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef WRITE_STATUS
#define WRITE_STATUS

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_IMG_NULL,
    WRITE_ERROR
    /* коды других ошибок  */
};
#endif
