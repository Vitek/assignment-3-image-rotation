//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef FROM_BMP
#define FROM_BMP

#include "bmp_header.h"
#include "read_status.h"
#include <image.h>
#include <stdint.h>
#include <stdio.h>
#include <util.h>

enum read_status from_bmp(FILE *in, struct image *img);

#endif
