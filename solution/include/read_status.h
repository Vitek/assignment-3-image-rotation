#pragma once

#ifndef READ_STATUS
//
// Created by vccha on 23.01.2023.
//

#define READ_STATUS

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NULL_IN,
    READ_CREATE_IMAGE_ERROR
    /* коды других ошибок  */
};

#endif
