//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef IMAGE
#define IMAGE

#include "pixel.h"
#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)

struct image {
    uint64_t height, width;
    struct pixel* data;
};

#pragma pack(pop)

size_t image_size(const struct image* img);

struct image create_empty_image(void);

struct image create_image(const uint64_t height, const uint64_t width);

struct pixel* allocate_image_data(uint64_t height, uint64_t width);

void free_image(struct image* image);

#endif
