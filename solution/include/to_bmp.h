//
// Created by vccha on 23.01.2023.
//

#pragma once

#ifndef TO_BMP
#define TO_BMP

#include "bmp_header.h"
#include "image.h"
#include "write_status.h"
#include <image.h>
#include <stdint.h>
#include <stdio.h>
#include <util.h>

enum write_status to_bmp(FILE* out, struct image const* img);

#endif
